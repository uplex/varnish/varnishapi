/*-
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package log

/*
#cgo pkg-config: varnishapi
#include <stdio.h>
#include <vapi/vsl.h>
*/
import "C"

import "unsafe"

const (
	identmask     = ^uint32(3 << 30)
	lenmask       = uint32(0x0ffff)
	clientmarker  = uint32(1 << 30)
	backendmarker = uint32(1 << 31)
	maxRecLen     = 4096
)

func (rec *C.struct_VSLC_ptr) tag() Tag {
	ptr := (*uint32)(unsafe.Pointer(rec.ptr))
	return Tag(uint8(*ptr >> 24))
}

func (rec *C.struct_VSLC_ptr) vxid() uint32 {
	ptr := (*[2]uint32)(unsafe.Pointer(rec.ptr))
	return ptr[1] & identmask
}

func (rec *C.struct_VSLC_ptr) length() uint32 {
	ptr := (*uint32)(unsafe.Pointer(rec.ptr))
	return *ptr & lenmask
}

func (rec *C.struct_VSLC_ptr) payload() []byte {
	length := rec.length() - 1
	ptr := (*[maxRecLen]byte)(unsafe.Pointer(uintptr(unsafe.Pointer(rec.ptr)) + 2*unsafe.Sizeof(*rec.ptr)))
	b := ptr[:length]
	bytes := make([]byte, length, length)
	copy(bytes, b)
	return bytes
}

func (rec *C.struct_VSLC_ptr) client() bool {
	ptr := (*[2]uint32)(unsafe.Pointer(rec.ptr))
	return ptr[1]&clientmarker != 0
}

func (rec *C.struct_VSLC_ptr) backend() bool {
	ptr := (*[2]uint32)(unsafe.Pointer(rec.ptr))
	return ptr[1]&backendmarker != 0
}
